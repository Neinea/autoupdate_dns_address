import os
import unittest

from unittest.mock import AsyncMock, Mock, call, patch
from autoupdate_dns_address.dns_manager import DnsManager


class DnsManagerTest(unittest.IsolatedAsyncioTestCase):
    async def asyncSetUp(self) -> None:
        self.mock_ovh_client = Mock()
        self.dns_manager = DnsManager(
            ovh_client=self.mock_ovh_client,
            domain="mock_domain",
            sub_domain="mock_subdomain",
            telegram_bot_token=None,
            telegram_chat_id=None,
        )

    async def asyncTearDown(self) -> None:
        self.mock_ovh_client.reset_mock()

    @patch(
        "autoupdate_dns_address.dns_manager.aiohttp.ClientSession",
    )
    async def test_read_public_ip_address(self, client_session):
        client_session.return_value = AsyncMock(
            __aenter__=AsyncMock(
                return_value=AsyncMock(
                    get=Mock(
                        return_value=AsyncMock(
                            __aenter__=AsyncMock(return_value=AsyncMock(text=AsyncMock(return_value="98.207.254.136")))
                        )
                    )
                )
            )
        )
        self.assertEqual(
            await self.dns_manager._read_public_ip_address(),
            "98.207.254.136",
        )

    async def test_has_ip_address_changed(self):
        self.dns_manager.ip_config_file = os.path.join(os.getcwd(), "tests", "test_dns_manager_config.txt")
        self.assertEqual(
            self.dns_manager._has_ip_address_changed("98.207.254.136", "98.207.254.136"),
            False,
        )

    async def test_set_new_address_for_ovh_dns(self):
        mock_get = AsyncMock(
            side_effect=[
                ["mock_service"],
                [123465789, 987654321],
                {"target": "45.56.78.98"},
                {"target": "1.2.3.4"},
            ]
        )
        mock_put = AsyncMock()
        self.mock_ovh_client.get = mock_get
        self.mock_ovh_client.put = mock_put
        await self.dns_manager._set_new_address_for_ovh_dns("1.2.3.4", "4.3.2.1")
        mock_get.assert_has_calls(
            [
                call("https://api.ovh.com/1.0/domain/zone"),
                call(
                    "https://api.ovh.com/1.0/domain/zone/mock_service/record",
                    {"fieldType": "A"},
                ),
                call("https://api.ovh.com/1.0/domain/zone/mock_service/record/123465789"),
                call("https://api.ovh.com/1.0/domain/zone/mock_service/record/987654321"),
            ],
        )
        mock_put.assert_called_once_with(
            "https://api.ovh.com/1.0/domain/zone/mock_service/record/987654321",
            {"target": "4.3.2.1"},
        )

    async def test_get_ip_related_to_domain(self):
        mock_get = AsyncMock(side_effect=[["123456789"], {"target": "1.2.3.4"}])
        self.mock_ovh_client.get = mock_get
        assert await self.dns_manager._get_ip_related_to_domain() == "1.2.3.4"
        mock_get.assert_has_calls(
            [
                call(
                    "https://api.ovh.com/1.0/domain/zone/mock_domain/record",
                    {"fieldType": "A", "subDomain": "mock_subdomain"},
                ),
                call("https://api.ovh.com/1.0/domain/zone/mock_domain/record/123456789"),
            ]
        )
