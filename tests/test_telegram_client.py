import unittest
from unittest.mock import AsyncMock

from autoupdate_dns_address.telegram_client import TelegramClient


class TelegramClientTest(unittest.IsolatedAsyncioTestCase):
    async def asyncSetUp(self) -> None:
        self.telegram_bot_mock = AsyncMock()
        self.telegram_client = TelegramClient(self.telegram_bot_mock, 123456)

    async def asyncTearDown(self) -> None:
        self.telegram_bot_mock.reset_mock()

    async def test_notify_dns_update(self):
        await self.telegram_client.notify_dns_update("1.2.3.4")
        self.telegram_bot_mock.send_message.assert_called_once_with(
            123456, "Your ip has changed and has been modified in ovh infrastucture. Your new ip is 1.2.3.4"
        )
        self.telegram_bot_mock.close.assert_called_once_with()
