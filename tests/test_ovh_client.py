import unittest
from unittest.mock import patch, AsyncMock, Mock

from autoupdate_dns_address.ovh_client import OvhClient


class OvhClientTest(unittest.IsolatedAsyncioTestCase):
    async def asyncSetUp(self) -> None:
        self.ovh_client = OvhClient(
            "mock_application_key",
            "mock_secret_application_key",
            "mock_secret_consumer_key",
        )

    @patch("autoupdate_dns_address.ovh_client.aiohttp.ClientSession")
    @patch("autoupdate_dns_address.ovh_client.time.time")
    async def test_get(self, frozen_time, client_session):
        frozen_time.return_value = 1647469498.2562
        session = AsyncMock(
            request=Mock(
                return_value=AsyncMock(
                    __aenter__=AsyncMock(
                        return_value=AsyncMock(
                            json=AsyncMock(return_value=["mock_domain_name"])
                        )
                    )
                )
            )
        )
        client_session.return_value = AsyncMock(
            __aenter__=AsyncMock(return_value=session)
        )
        self.assertEqual(await self.ovh_client.get("mock_path"), ["mock_domain_name"])

    @patch("autoupdate_dns_address.ovh_client.aiohttp.ClientSession")
    @patch("autoupdate_dns_address.ovh_client.time.time")
    async def test_put(self, frozen_time, client_session):
        frozen_time.return_value = 1647469498.2562
        session = AsyncMock(
            request=Mock(
                return_value=AsyncMock(
                    __aenter__=AsyncMock(
                        return_value=AsyncMock(json=AsyncMock(return_value=None))
                    )
                )
            )
        )
        client_session.return_value = AsyncMock(
            __aenter__=AsyncMock(return_value=session)
        )
        self.assertEqual(await self.ovh_client.put("mock_path", {}), None)
