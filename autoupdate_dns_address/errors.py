class AutoUpdateDnsError(Exception):
    pass


class RecordNotFoundError(AutoUpdateDnsError):
    pass