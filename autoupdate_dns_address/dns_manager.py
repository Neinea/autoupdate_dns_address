import os
from dataclasses import dataclass
from datetime import datetime
from typing import List

import aiohttp
from aiofile import async_open
from aiogram import Bot

from autoupdate_dns_address.errors import RecordNotFoundError
from autoupdate_dns_address.ovh_client import OvhClient
from autoupdate_dns_address.telegram_client import TelegramClient


@dataclass
class DnsManager:
    ovh_client: OvhClient
    domain: str
    sub_domain: str | None
    telegram_bot_token: str | None
    telegram_chat_id: int | None
    ip_config_file: str = "/tmp/dns_manager_config.txt"

    async def run(self) -> None:
        start = datetime.now()
        if not os.path.isfile(self.ip_config_file):
            ip_address = await self._get_ip_related_to_domain()
            await self._update_current_ip_address_in_local_file(ip_address)
        public_ip_address = await self._read_public_ip_address()
        old_ip_address = await self._read_ip_address_from_local_file()
        if self._has_ip_address_changed(old_ip_address, public_ip_address):
            await self._set_new_address_for_ovh_dns(old_ip_address, public_ip_address)
            await self._update_current_ip_address_in_local_file(public_ip_address)
            if self.telegram_bot_token and self.telegram_chat_id:
                telegram_client = TelegramClient(Bot(self.telegram_bot_token), self.telegram_chat_id)
                await telegram_client.notify_dns_update(public_ip_address)
        print("It takes {} seconds to do the operation".format(datetime.now() - start))

    async def _read_public_ip_address(self) -> str:
        async with aiohttp.ClientSession(raise_for_status=True) as session:
            async with session.get("https://api.ipify.org") as response:
                return await response.text()

    async def _read_ip_address_from_local_file(self) -> str:
        async with async_open(self.ip_config_file) as afp:
            return await afp.read()

    async def _get_ip_related_to_domain(self) -> str:
        params = {"fieldType": "A"}
        if self.sub_domain:
            params["subDomain"] = self.sub_domain
        records = await self.ovh_client.get(
            "https://api.ovh.com/1.0/domain/zone/{}/record".format(self.domain),
            params,
        )
        if not records:
            raise RecordNotFoundError("The domain {} has no record".format(self.domain))

        record_details = await self.ovh_client.get(
            "https://api.ovh.com/1.0/domain/zone/{}/record/{}".format(self.domain, records[0])
        )
        return record_details["target"]

    async def _set_new_address_for_ovh_dns(self, old_public_ip_address: str, current_public_ip_address: str) -> None:
        available_services: List[str] = await self.ovh_client.get("https://api.ovh.com/1.0/domain/zone")
        for available_service in available_services:
            records = await self.ovh_client.get(
                "https://api.ovh.com/1.0/domain/zone/{}/record".format(available_service),
                {"fieldType": "A"},
            )
            affected_records: List[int] = []
            for record in records:
                record_details = await self.ovh_client.get(
                    "https://api.ovh.com/1.0/domain/zone/{}/record/{}".format(available_service, record)
                )
                if record_details.get("target") == old_public_ip_address:
                    affected_records.append(record)

            for affected_record in affected_records:
                await self.ovh_client.put(
                    "https://api.ovh.com/1.0/domain/zone/{}/record/{}".format(available_service, affected_record),
                    {"target": current_public_ip_address},
                )

    async def _update_current_ip_address_in_local_file(self, current_public_ip_address: str) -> None:
        async with async_open(self.ip_config_file, "w+") as file:
            await file.write(current_public_ip_address)

    def _has_ip_address_changed(self, old_public_ip_address: str, current_public_ip_address: str) -> bool:
        return old_public_ip_address != current_public_ip_address
