from dataclasses import dataclass
import hashlib
import json
import time
import urllib.parse
from typing import Any, Dict, Literal
import aiohttp


@dataclass
class OvhClient:
    application_key: str
    secret_application_key: str
    secret_consumer_key: str

    async def get(self, path: str, params: Dict[str, Any] | None = None) -> Any:
        return await self._internal_query("GET", path, None, params)

    async def put(self, path: str, json: Dict[str, Any], params: Dict[str, Any] | None = None) -> Any:
        return await self._internal_query("PUT", path, json, params)

    async def _internal_query(
        self,
        method_type: Literal["GET", "PUT"],
        path: str,
        data: Dict[str, Any] | None,
        params: Dict[str, Any] | None,
    ) -> Any:
        body = ""
        now = str(int(time.time()))
        headers = {
            "X-Ovh-Application": self.application_key,
            "X-Ovh-Consumer": self.secret_consumer_key,
            "X-Ovh-Timestamp": now,
        }
        if data is not None:
            headers["Content-Type"] = "application/json"
            body = json.dumps(data)

        formatted_path = self._format_path(path, params)
        sha1_signature = hashlib.sha1(
            "+".join(
                [
                    self.secret_application_key,
                    self.secret_consumer_key,
                    method_type,
                    formatted_path,
                    body,
                    now,
                ]
            ).encode("utf-8")
        )
        headers["X-Ovh-Signature"] = "$1${}".format(sha1_signature.hexdigest())

        async with aiohttp.ClientSession(raise_for_status=True) as session:
            async with session.request(method_type, path, headers=headers, data=body, params=params) as response:
                return await response.json()

    def _format_path(self, path: str, params: Dict[str, Any] | None) -> str:
        if not params:
            return path
        return "{}?{}".format(path, urllib.parse.urlencode(params))
