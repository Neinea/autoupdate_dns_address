from dataclasses import dataclass

from aiogram import Bot


@dataclass(frozen=True)
class TelegramClient:
    token_bot: Bot
    chat_id: int

    async def notify_dns_update(self, new_ip_address: str) -> None:
        try:
            await self.token_bot.send_message(
                self.chat_id,
                "Your ip has changed and has been modified in ovh infrastucture. Your new ip is {}".format(
                    new_ip_address
                ),
            )
        finally:
            await self.token_bot.close()  #  type: ignore
