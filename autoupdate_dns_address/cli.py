import asyncio
import os

import typer

from autoupdate_dns_address.dns_manager import DnsManager
from autoupdate_dns_address.ovh_client import OvhClient

app = typer.Typer()


@app.command()
def start(
    domain: str,
    sub_domain: str = typer.Option(""),
    ip_config_file: str = typer.Option("/tmp/dns_manager_config.txt"),
    application_key: str = typer.Option(""),
    secret_application_key: str = typer.Option(""),
    secret_consumer_key: str = typer.Option(""),
    telegram_bot_token: str = typer.Option(""),
    telegram_chat_id: int = typer.Option(0),
):
    manager = DnsManager(
        OvhClient(
            application_key or os.environ["OVH_APPLICATION_KEY"],
            secret_application_key or os.environ["OVH_SECRET_APPLICATION_KEY"],
            secret_consumer_key or os.environ["OVH_SECRET_CONSUMER_KEY"],
        ),
        domain,
        sub_domain,
        telegram_bot_token,
        telegram_chat_id,
        ip_config_file,
    )
    asyncio.run(manager.run(), debug=True)
